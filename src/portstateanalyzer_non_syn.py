#!bin/python3.8

import src.portstateanalyzer as analyzer
from scapy import all as scapy

class PortStateAnalyzer_NON_SYN(analyzer.PortStateAnalyzer):
    """
        A class to analyze port state based on the flags from the response from target.
        It holds the logic for scans that don't start with the SYN packet.
        
        Methods
        -------
        analyze(response)
            Analyze the response and return the port state
    """

    def __init__(self):
        pass

    def analyze(self, response):
        """
            Look on the flags of the response from the target and guess for the port state.

            Parameters
            ----------
            response : scapy
                The response from the target
        """

        if(response is None): # Port is opne if no response returned
            return "Open / filtered"
        elif(response.haslayer(scapy.TCP)): # If it has TCP layer analyze it
             flags = reseponse[scapy.TCP].flags
            
             if(flags == "R"): # Port is closed
                return "Closed"
             
             return flags # Unknown state
        elif(response.haslayer(scapy.ICMP)): # Port is filterd
             return "Filtered"
        else:
             return response.summary()
