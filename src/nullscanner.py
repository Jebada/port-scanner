#!python3.8

from scapy import all as scapy
import src.scanner as scanner
import src.packetfactory as packetfactory
import src.portstateanalyzer_non_syn as analyzer
class NULLScanner(scanner.Scanner):
    """
        This class is a NULL port scanner. It sends packet with the NULL flag to the rarget and
        analyze the port state based on reponse.

        Attributes
        ----------
        packet_factory : PacketFactory
            instance of a packetfactory class that gives the packets to send
        response_analyzer : PortStateAnalyzer_NON_SYN
            analyze the port state based on reponse from the target

        Methods
        -------
        scan(ip_address, port, response_timeout)
    """
    
    packet_factory = packetfactory.PacketFactory.instance()
    response_analyzer = analyzer.PortStateAnalyzer_NON_SYN()

    def __init__(self):
        pass

    def scan(self, ip_address, port, response_timeout):
        """
            Sends packet to the target with the FIN flag set. Then analyze the port state based on
            response from the target

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                port number to scan
            response_timeout : int
        """
        null_packet = self.packet_factory.get_null_packet(ip_address, port) # Get the NULL packet
        response = scapy.sr1(null_packet, timeout=response_timeout) # send the packet

        port_state = self.response_analyzer.analyze(response) # Analyze the packet

        return (port, port_state)
