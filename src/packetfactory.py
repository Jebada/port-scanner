#!bin/python3.8
import random
from scapy import all as scapy

class PacketFactory():
    """
        A singleton class that return packet to send.

        Attributes
        ----------
        _instance : PacketFactory
            This is the only instance of the PacketFactory that will be used
        
        Methods
        -------
        instance()
            Return the singleton. If instance is not initialized it will initialize it
        get_syn_packet(ip_address, port)
            return packet with SYN flag set
        get_generic_packet(ip_address, port, synack_packet, request_flags):
            return packet with flags set to request_flags. Sequence number is calculated from ACK
            number of the syn_packet.
        get_xmas_packet(ip_address, port)
            return packet with the XMAS flags
        get_null_packet(ip_address, port)
            return packet without flags
        get_fin_packet(ip_address, port)
            return packet with FIN flag set
    """

    # Singleton
    _instance = None

    # Like a private constructor
    def __init__(self):
        raise RuntimeException('Call instance() instead')

    @classmethod
    def instance(cls):
        """
            Return the singleton instance of PacketFactory. If the instance was not initialized yet
            then it will create the instance.
        """

        if cls._instance is None:
            print('Creating new PacketFactory')
            cls._instance = cls.__new__(cls)

        return cls._instance
    
    def get_syn_packet(self, ip_address, port):
        """
            Method that return packet with the SYN flag.
            First generate random destination port. Then generate random SEQ number.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                destination port number
        """

        # Generate random port
        random_sport = random.randint(1000, 2000)
        seq_number = random.randrange(0,(2**32)-1)
        # return the packet
        return scapy.IP(dst=ip_address)/scapy.TCP(dport=[port],sport=[random_sport],seq=seq_number,flags="S")

    def get_generic_packet(self, ip_address, port, synack_packet, request_flags):
        """
            Method that return packet with the custom flags.
            First get the SEQ number expected. Then calculate the ACK number.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                destination port number
            synack_packet : ?
                reponse from the target on the SYN packet
            request_flags : str
                flags that the packet should have set
        """

        seq_number = synack_packet.ack # Get sequence number
        ack_number = (synack_packet.seq + 1) # Calculate ACK number
        
        # return RST packet
        return scapy.IP(dst=ip_address)/scapy.TCP(dport=[port], flags=request_flags, seq=seq_number, ack=ack_number)

    def get_xmas_packet(self, ip_address, port):
        """
            Method that return packet with the FIN, PUSH and URG flags.
            First generate random destination port. Then generate random SEQ number.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                destination port number
        """
        # Generate random port and seq number
        random_sport = random.randint(1000,2000)
        seq_number = random.randrange(0,(2**32)-1)
        
        return scapy.IP(dst=ip_address)/scapy.TCP(dport=[port], flags="FPU", seq=seq_number)

    def get_null_packet(self, ip_address, port):
        """
            Method that return packet with no flags.
            First generate random destination port. Then generate random SEQ number.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                destination port number
        """
        # Generate random port and seq number
        random_sport = random.randint(1000,2000)
        seq_number = random.randrange(0,(2**32)-1)

        return scapy.IP(dst=ip_address)/scapy.TCP(dport=[port], flags='', seq=seq_number)

    def get_fin_packet(self, ip_address, port):
        """
            Method that return packet with the FIN flag.
            First generate random destination port. Then generate random SEQ number.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                destination port number
        """
        # Generate random port and seq number
        random_sport = random.randint(1000, 2000)
        seq_number = random.randrange(0,(2**32)-1)

        return scapy.IP(dst=ip_address)/scapy.TCP(dport=[port], flags="F", seq=seq_number)
