#!bin/python3.8

from scapy import all as scapy
import src.scanner as scanner
import src.packetfactory as packetfactory
import src.portstateanalyzer_syn as analyzer
class SYNScanner(scanner.Scanner):
    """
        This class is a SYN port scanner. It sends packet with the SYN flag to the rarget and
        analyze the port state based on reponse.

        Attributes
        ----------
        packet_factory : PacketFactory
            instance of a packetfactory class that gives the packets to send
        response_analyzer : PortStateAnalyzer_NON_SYN
            analyze the port state based on reponse from the target

        Methods
        -------
        scan(ip_address, port, response_timeout)
            scan the port
        send_rst_packet(response, port, ip_address)
            send the RST flag to the target

    """

    packet_factory = packetfactory.PacketFactory.instance()
    response_analyzer = analyzer.PortStateAnalyzer_SYN()

    def __init__(self):
        pass

    def scan(self, ip_address, port, response_timeout):
        """
            Sends packet to the target with the SYN flag set. Then analyze the port state based on
            response from the target.
            If the port is open then send the RST flag to abbort the connection.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                port number to scan
            response_timeout : int
        """
        syn_packet = self.packet_factory.get_syn_packet(ip_address, port) # Get syn packet
        response = scapy.sr1(syn_packet, timeout=response_timeout) # Try to connect to the socket and send the packet
        print(type(response).__name__)
        if response is None: # TIMEOUT was reached
            return (port, "TIMEOUT")

        flags = response.getlayer(scapy.TCP).flags # Harvest the flags from response
        port_state = response_analyzer.analyze(flags) # Get port state

        if(port_state == "Open"):
            self.send_rst_packet(response, port, ip_address)

        return (port, port_state)


    def send_rst_packet(self, response, port, ip_address):
        """
            Send RST packet back to the target

            Parameters
            ----------
            reponse : scapy.
                response from the target. It is used to calculate the SEQ number
            port : int
                port number that is scanned
            ip_address : str
                IP address of the target
        """
        
        rst_packet = self.packet_factory.get_generic_packet(ip_address, port, response, "R") # Ask packet factory for RST packet
        scapy.send(rst_packet)  # Send the RST packet
