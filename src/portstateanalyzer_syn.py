#!bin/python3.8
import src.portstateanalyzer as analyzer

class PortStateAnalyzer_SYN(analyzer.PortStateAnalyzer):
    """ 
        This class analyze the port state based on the flags in response from the target.
        It is used for scans that starts with the SYN flags.

        Methods
        -------
        analyze(flag)
            Analyze the flags and return port state
    """

    def __init__(self):
        pass

    def analyze(self, flag):
        """
            Analyze the flag and return guess of the port state.

            Parameters
            ----------
            flag : str
                The flags from the response from the target
        """
        
        if(flag == "SA"):
            return "Open"
        elif(flag == "R"):
            return "Closed"
        elif(flag is None):
            return "Filtered"
        else:
            return "STRANGE"

