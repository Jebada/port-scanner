#!/bin/python3.8

from abc import ABC, abstractmethod
class Scanner(ABC):
    """
        This is abstract class for the scanners.

        Methods
        -------
        scan(ip_address, port)
            scan the ip address port
    """
    
    def scan(self, ip_address, port):
        """
            Scan the target port.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                Port number to scan
        """
        pass
