#!bin/python3.8

from abc import ABC, abstractmethod
class PortStateAnalyzer():
    """
        Abstract class for port state analyzers

        Methods
        -------
        analyze(flag)
            Analyze the port state based on the flags from target response
    """

    def analyze(self, flag):
        """
            Analyze the port state based on the flags from target response.

            Parameters
            ----------
            flag : str
                flags from the response from the target
        """
        pass
