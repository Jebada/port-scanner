#!bin/python3.8
import concurrent.futures

class PortScanner():
    """
        A class that has the scanner instance and performes the scanning

        Attributes
        ----------
        scanner : Scanner
            Actual scanner

        Methods
        -------
        scan(ip_adress, timeout)
            Create threads and call scanner.scan() method.
    """

    def __init__(self, scanner):
        """
            Parameters
            ----------
            scanner : Scanner
                Actual scanner
        """

        self.scanner = scanner # Scanner that will be used

    
    def scan(self, ip_adress, timeout):
        """
            Start the ThreadPoolExecutor and call the scanner with the port number to scan.
            Then collect the port states and return.

            Parameters
            ----------
                ip_adress : str
                    The IP address of the target
                timeout : int
                    How long try to connect

                Raises
                ------
                KeyboardInterrupt
                    If the scan is interrupted by user 
        """

        results = [] # List to be returned

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = []
            for port in range(450, 455): # Start thread scanning
                futures.append(executor.submit(self.scanner.scan, ip_adress, port, timeout))
            try:

                for future in concurrent.futures.as_completed(futures): # Collect the results
                    if not future.result(): # Don't collect None results
                        pass
                    else:
                        results.append(future.result())
            except KeyboardInterrupt:
                executor._threads.clear()
                concurrent.futures.thread._threads_queues.clear()
        return results
