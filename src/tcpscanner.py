#!bin/python3.8

import src.scanner as scanner
import src.packetfactory as packetfactory
from scapy import all as scapy
import src.portstateanalyzer_syn as analyzer
class TCPScanner(scanner.Scanner):
    """
        This class is a SYN port scanner. It sends packet with the SYN flag to the rarget and
        analyze the port state based on reponse.

        Attributes
        ----------
        packet_factory : PacketFactory
            instance of a packetfactory class that gives the packets to send
        response_analyzer : PortStateAnalyzer_NON_SYN
            analyze the port state based on reponse from the target

        Methods
        -------
        scan(ip_address, port, response_timeout)
            scan the port
        finish_tcp_handshake(response, port, ip_address)
            send ACK and FIN packets to the target
    """
    packet_factory = packetfactory.PacketFactory.instance()
    response_analyzer = analyzer.PortStateAnalyzer_SYN()

    def __init__(self):
        pass

    def scan(self, ip_address, port, response_timeout):
        """
            Sends packet to the target with the SYN flag set. Then analyze the port state based on
            response from the target.
            If the port is open then finish the handshake.

            Parameters
            ----------
            ip_address : str
                IP address of the target
            port : int
                port number to scan
        """

        syn_packet = self.packet_factory.get_syn_packet(ip_address, port) # Get SYN packet from PacketFactory
        response = scapy.sr1(syn_packet, timeout=response_timeout) # Send the SYN packet
       
        if response is None: # Timeout was reached
            return (port, "TIMEOUT")
        
        flags = response.getlayer(scapy.TCP).flags # Harvest the flags from response

        port_state = response_analyzer.analyze(flags)

        if(port_state == "Open"):
            self.finish_tcp_handshake(response, port, ip_address)
        
        return (port, port_state)


    def finish_tcp_handshake(self, response, port, ip_address):
        """
            Finish the TCP handshake. So send the ACK flag and then the FIN flag.
            If the connection timeouts then return as we wanna close the session anyway

            Parameters
            ----------
            response : 
                response from the target. Is used to calculate SEQ number
            port : int
                port number the session is on
            ip_address : string
                IP address of the target
        """
        # Send ACK packet
        ACK_packet = self.packet_factory.get_generic_packet(ip_address, port, response, "A")
        ACK_response = scapy.sr1(ACK_packet, timeout = 2)
        if ACK_response is None:
            return 
        # SEND the FIN
        scapy.send(self.packet_factory.get_generic_packet(ip_address, port, ACK_response, "F"),
                  timeout = 2)
