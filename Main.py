#!bin/python3.8
"""

    This program scan the target ip address ports.
    It can use five well-known sannners: SYN scann, full TCP scan, NULL scan, FIN scan and XMAS scan.

    The script accept ip addres of the target(NOT optional), scan type(default is SYN scan) and
    timeout(default 5)

    The script require scapy to be installed within the python environment that the script runs in.
"""

import argparse
import sys

import src.tcpscanner as tcpscanner
import src.xmasscanner as xmasscanner
import src.synscanner as synscanner
import src.nullscanner as nullscanner
import src.finscanner as finscanner
import src.portscanner as portscanner

def get_scanner(scan_type):
    """
        Helper methods for scan resolution. Look at the parameter and return appropriate scanner
        object.

        Parameters
        ----------
        scan_type : str
            String interpretation of the scan
    """

    if scan_type == 'SYN':
        return synscanner.SYNScanner()
    elif scan_type == 'TCP':
        return tcpscanner.TCPScanner()
    elif scan_type == 'NULL':
        return nullscanner.NULLScanner()
    elif scan_type == 'XMAS':
        return xmasscanner.XMASScanner()
    elif scan_type == 'FIN':
        return finscanner.FINScanner()
    else:
        raise TypeError('Unknown scanner type')

def print_results_to_console(results):
    """
        Helper method that prints the scan results to the console.

        Parameters
        ----------
        results : List
            List of tuples that holds the port number and port state
    """

    for port, state in results:
        print('Port {} ---------> {}'.format(port, state))

def main():
    # Read arguments and resolve which scanner should be used and what IP to scan
    parser = argparse.ArgumentParser(description='Scan target for open ports')

    # Add accepted argumets
    parser.add_argument('-i', '--ip-address', required=True, help='IP address of the target')
    parser.add_argument('-t', '--timeout', default=5, type=int, help='How long to wait for reponse from target')
    parser.add_argument('-s', '--scan-type', default='SYN', choices=['SYN', 'TCP', 'NULL', 'XMAS',
                                                                     'FIN'], help='What type of'
                        + ' scan to use. Default: SYN scan')
    args = parser.parse_args() # Extract the arguments

    scanner = get_scanner(args.scan_type)
    port_scanner = portscanner.PortScanner(scanner) # initrialize the PortScanner
    results = port_scanner.scan(args.ip_address, args.timeout)
    
    print_results_to_console(results)

if (__name__ == "__main__"):
    main()
