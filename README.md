<p>
<h1>Port Scanner</h1>
Very basic portscanner supporting SYN, TCP, FIN, NULL and XMAS scan.
<br>
<h2>Installation</h2>
<pre>pip -r install requirements.txt</pre>
<br>
<h2>Usage</h2>
<pre>
Main.py [-h] -i IP_ADDRESS [-t TIMEOUT] [-s {SYN,TCP,NULL,XMAS,FIN}]

Scan target for open ports

optional arguments:
  -h, --help            show this help message and exit
  -i IP_ADDRESS, --ip-address IP_ADDRESS
                        IP address of the target
  -t TIMEOUT, --timeout TIMEOUT
                        How long to wait for reponse from target
  -s {SYN,TCP,NULL,XMAS,FIN}, --scan-type {SYN,TCP,NULL,XMAS,FIN}
                        What type of scan to use. Default: SYN scan
</pre>
<p/>

